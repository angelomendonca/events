# Sguil Events Reader

## Steps to get started.
1. Make sure you have ruby 1.9.3 installed. I used Security Onion Ubuntu 14.04 to get started.

2. Execute `gem install bundle` followed by `bundle install` in the current git checkout directory. This will install all the gems needs.
3. Please have a look at the events.yml before installing the ruby process. It has all the 
	params needed for the event program to read from the snoby/sguil mysql db and send post
	requests to the rails server. I have not used https which will be a further improvement.
4. Make sure that the rails server is accessible and that the schema for the MySql Events
	has been created.

## Installation

5. Make sure you have installed the gems before installing the ruby program. 

	To install all the gems run the following commands in the checked out directory.
	`'gem install bundle` followed by  `'bundle install`.

	To install the events application on Ubuntu 14.04 run 
	`sudo rake install` and to uninstall `sudo rake uninstall`.


## Debugging after installation incase of errors in /var/log/events.log

1. Make sure you have the right settings in the /etc/events/events.yml file
2. The rails server should be accessible. If the firewall is denying outbound
	connection on the port your rails server is running on make sure to allow
	them. 
3. Another issue could be the process not able to connect to the snorby/sguil 
	mysql database. Make sure the user has the right grant permissions and also
	has access to the mysql process. The port could be blocked by the firewall.