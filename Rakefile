# Make sure to run as sudo or as root
task :install do
  sh <<-EOS.strip_heredoc, {verbose: true}
    /bin/bash -xeuo pipefail <<'BASH'
    echo "Installing Events reader!"

    if [[ -e /bin/events.rb ]]; then
        echo 'Events reader is already installed'
        exit
    fi

    cp events.rb /bin/events.rb
    chown `whoami`:`whoami` /bin/events.rb

    if [[ ! -e /etc/events ]]; then
        mkdir /etc/events
    fi

    chmod 0775 /etc/events
    cp events.yml /etc/events/events.yml
    if [[ ! -e /var/log/events ]]; then
        mkdir /var/log/events
    fi

    chmod 0755 /var/log/events
    chown -R `whoami`:`whoami` /var/log/events

    cp events.conf /etc/init/events.conf
    touch /etc/events/timestamp
    chmod 0775 /etc/events/timestamp
    chown -R `whoami`:`whoami` /etc/events/

    echo "Installing Events reader!"

    if (( $(ps -ef | grep -v grep | grep '/bin/events.rb' | wc -l) == 0 )); then
        echo "Starting Events service!!!"
        sudo service events start
    fi

    BASH
  EOS
end

task :uninstall do
  sh <<-EOS.strip_heredoc, {verbose: false}
    /bin/bash -xeuo pipefail <<'BASH'
    echo "Uninstalling Events reader!"

    if (( $(ps -ef | grep -v grep | grep '/bin/events.rb' | wc -l) > 0 )); then
        echo "Stopping Events service!!!"
        sudo service events stop
    fi
    OUT=$?
    if [[ $OUT -eq 1 ]];then
        kill -9 `pgrep -f /events.rb`
    fi

    if [[ -e /bin/events.rb ]]; then
        rm /bin/events.rb
    elif [[ ! -e /bin/events ]]; then
        echo 'Events reader is not installed'
    fi

    if [[ -e /etc/init/events.conf ]]; then
        rm /etc/init/events.conf
    fi

    if [[ -e /etc/events ]]; then
        rm -rf /etc/events
    fi

    if [[  -e /var/log/events ]]; then
        rm -rf /var/log/events
    fi

    if [[ -e /etc/events/timestamp ]]; then
        rm /etc/events/timestamp
    fi
    echo "UnInstalling Events reader Completed!"
    BASH
  EOS
end

class String
  def strip_heredoc
    gsub(/^#{scan(/^[ \t]*(?=\S)/).min}/, ''.freeze)
  end
end
