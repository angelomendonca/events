require 'json'
require 'logger'
require 'mysql'
require 'httparty'
require "yaml"

$JSON_HEADER = {'Content-Type' => 'application/json'}
$CONFIG = YAML.load_file '/etc/events/events.yml'
app_server = $CONFIG['host']

URL_LOCATION = "#{app_server['url']}:#{app_server['port']}/events"

begin
  log_file = File.open("#{$CONFIG['log_file']}", "a")
rescue Exception => e
  log_file = File.new("#{$CONFIG['log_file']}", "a")
end
log_file.sync = true
$LOGGER = Logger.new(log_file)


def post_event_data(json_data, timestamp)
  begin
    response = HTTParty.post(URL_LOCATION, body: json_data,
                             headers: $JSON_HEADER)
    if response.code.to_i == 201
      save_timestamp(timestamp)
    else
      $LOGGER.debug "#{response.body}"
    end
  rescue Exception => e
    $LOGGER.error "Rest Server Error !: #{e}"
  end
end


def process_events(timestamp)
  # load the below params from a CONFIG file or OS environ
  db = $CONFIG['database']
  begin
    connection = Mysql.new(db['host'], db['username'], db['password'], db['name'])
    if timestamp.nil?
      sql_string = "SELECT * FROM event ORDER BY timestamp LIMIT #{db['select_limit']};"
    else
      sql_string = "SELECT * FROM event WHERE timestamp > '#{timestamp}'
                    ORDER BY timestamp LIMIT #{db['select_limit']};"
    end

    result_set = connection.query(sql_string)
    rows = result_set.num_rows
    $LOGGER.debug "#{sql_string} #{rows}"

    result_set.each_hash do |row|
      row_timestamp = row['timestamp']
      row['clas'] = row.delete('class')
      row['format'] = 'json'
      post_data = JSON.generate(row)
      post_event_data(post_data, row_timestamp)

    end
  rescue Exception => e
    $LOGGER.error "Error !: #{e}"
  ensure
    connection.close if connection
  end
end


def get_timestamp
  begin
    timestamp = File.read("#{$CONFIG['timestamp_file']}") if 
                          File.exist?("#{$CONFIG['timestamp_file']}")
    return timestamp unless timestamp.nil?
    return nil
  rescue Exception => e
    $LOGGER.error "Cannot read timestamp file! #{e}"
  end
end


def save_timestamp(timestamp)
  begin
    timestamp_file = File.new("#{$CONFIG['timestamp_file']}", "w+")
    timestamp_file.print(timestamp)
    timestamp_file.close
  rescue Exception => e
    $LOGGER.error "Cannot save timetamp file! #{e}"
  end
end


while true
  loop do
    sleep $CONFIG['sleep_time']
    process_events(get_timestamp)
  end
end
